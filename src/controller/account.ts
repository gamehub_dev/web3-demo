import Web3 = require("web3");
import {IRouterContext} from "koa-router";

// HttpProvider의 host파라미터가 이더리움 노드에 접속하는 주소임
// https://web3js.readthedocs.io/en/1.0/getting-started.html
const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8546"));

export default class AccountController {

    // account 생성은 web.eth 모듈의 create 메소드를 사용
    // https://web3js.readthedocs.io/en/1.0/web3-eth-accounts.html#create
    // 리턴 값은 Account (typescript definition 참조)
    // 주의점읜 rpc콜을 받은 노드에 account가 남아 있지는 않음 (eth.accounts를 조회해 보면 해당 account가 없음)
    // 따라서 리턴되는 키 값을 반드시 관리 해야함
    public static async create(ctx: IRouterContext) {
        let newAccount = web3.eth.accounts.create();
        console.log(newAccount);

        ctx.status = 201;
        ctx.body = newAccount;
    }

    // account 생성은 web.eth 모듈의 create 메소드를 사용
    // https://web3js.readthedocs.io/en/1.0/web3-eth-personal.html#newaccount
    // 리턴 값은 account의 주소임
    // 키에 대한 정보가 리턴되지 않는 이유는 rpc콜을 받은 노드에 남아 있기 때문에
    // account 생성시에 사용한 password를 반드시 관리해야함.
    // 노드 구성에 따라 account 생성 방법이 다르수 있지만 어느 방식을 사용하던 password와 key가 사용자에게 노출되지 않게 매우 조심해야함
    public static async createInNode(ctx: IRouterContext) {
        let passphrase = (ctx.request.body as any).password;
        let newAddress = await web3.eth.personal.newAccount(passphrase);
        console.log(newAddress);

        ctx.status = 201;
        ctx.body = newAddress;
    }

    // account의 잔고 조회는 web.eth모듈의 getBalance 메소드를 사용
    // https://web3js.readthedocs.io/en/1.0/web3-eth.html#getbalance
    // 리턴값은 string임
    // 이더리움에서 어카운트는 어드레스의 랩퍼와 같다고 볼 수 있음
    // ethereum-go의 account 정의는 다음과 같다
    //     type Account struct {
    //         Address common.Address `json:"address"` // Ethereum account address derived from the key
    //         URL     URL            `json:"url"`     // Optional resource locator within a backend
    //     }
    public static async getBalance(ctx: IRouterContext) {
        const address: string = ctx.params.address;
        // !!! 주의 !!!
        // 타입 스크립트의 리턴 타입의 정의가 잘못되어 있음. 리턴 값은 number이지만 잘못된 타입 정의임 문서에 따르면 string이 맞음
        // 따라서 string -> BigNumber 변환이 필요함
        let balance = await web3.eth.getBalance(address);

        // return OK status code and loaded user object
        ctx.status = 200;
        ctx.body = web3.utils.fromWei(balance, "wei");
    }

}