import Web3 = require("web3");
import {IRouterContext} from "koa-router";
import {TransactionReceipt} from "web3/types";
import {Tx} from "web3/eth/types";
import {Signature} from "web3/eth/accounts";

// HttpProvider의 host파라미터가 이더리움 노드에 접속하는 주소임
// https://web3js.readthedocs.io/en/1.0/getting-started.html
const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8546"));
const GWEI: number = 1000000000;

export default class WithdrawController {

    private static async estimateGas(tx: Tx): Promise<number> {
        return await web3.eth.estimateGas(tx);
    }

    // 출금 요청을 받는 api 서버에 이더리움 노드가 존재하고 이 노드 월렛에 출금 계좌가 있는 경우
    public static async withdrawFromCurentNode(ctx: IRouterContext) {
        let withdrawAddress = (ctx.request.body as any).address;

        // // 해당 노드의 account 조회 - 출금 account를 알고 있다면 조회할 필요 없음
        let accounts = await web3.eth.getAccounts();

        // account[0]이 출금 계좌라고 가정
        // 출금 account에서 트랜잭션을 쓸수 있게 unlock
        // !!! typescript 잘못된 타입 정의
        // https://web3js.readthedocs.io/en/1.0/web3-eth-personal.html#unlockaccount
        // node_modules/@types/web3/eth/types.d.ts 51번째 줄을 다음과 같이 변경
        // unlockAccount(): void -> unlockAccount(account: string, passphrase: string, duration: number): void
         const isUnlocked = await web3.eth.personal.unlockAccount(accounts[0], "playcoingaza", 600);

        // typescript에서 javsacript 오브젝트를 Tx 타입으로 사용
        // 해당 노드 월렛에 account가 하나인 경우 from 생략 가능
        let tx = <Tx> {
            from: accounts[0],
            to: withdrawAddress,
            value: "50000000000000000", // 0.05 eth,
        };

        // 생성된 Transaction의 가스값을 측정
        // 이더 트랜잭션의 경우에는 보통 21000임
        const estimatedGas = await web3.eth.estimateGas(tx);
        tx.gas = estimatedGas;
        // gasPrice는 네트워크 혼잡도에 따라서 조정되어야 함
        // https://etherscan.io/gastracker 또는 https://ethgasstation.info를 살펴보기 바람
        // 두 싸이트 모두 간간히 죽음
        tx.gasPrice = 4 * GWEI;


        // 예를 들어 네트워크가 붐벼서 평균 가스비가 높아져서 트랜잭션이 pending상태에
        // 걸린다면 sendTransaction은 timeout이 날 수 있음
        let txReceipt = await web3.eth.sendTransaction(tx);

        if (txReceipt.status) {
            ctx.status = 200;
        } else {
            ctx.status = 400;
        }
        ctx.body = txReceipt.transactionHash;
    }

    // 게임에 상관 없이 보상을 지급은 정해져 있는 계좌에서 사용자에게 지급되어야 하므로
    // 트랜잭션에 해당 계좌의 privateKey를 사용해서 사인을 해서 노드에 보내야함
    // 간단하게 예를 들면 frank의 계좌에서 100달러를 donam의 계좌로 지급해달라는 트랜잭션을
    // donam이 생성할 수 없음. 하지만 donam이 frank의 privatekey를 가지고 사인을 하면 해당 트랜잭션은 적합한 트랜잭션이 됨.
    // 노드 구성에 따라 withdrawFromCurentNode와 withdrawFromRemoteNode의 사용이 선택되어 짐
    public static async withdrawFromRemoteNode(ctx: IRouterContext) {
        let withdrawAddress = (ctx.request.body as any).address;

        // 출금 주소를 '0x772Ddbb6A1818f69c76cd5392142E23Ca3C7E233'이고 이 주소에 대한 private를 가지고 있는 경우
        let tx = <Tx> {
            from: "0x772Ddbb6A1818f69c76cd5392142E23Ca3C7E233",
            to: withdrawAddress,
            value: "50000000000000000", // 0.05 eth,
        };

        const estimatedGas = await web3.eth.estimateGas(tx);
        tx.gas = estimatedGas;
        tx.gasPrice = 4 * GWEI;


        let signedTx: any = await web3.eth.accounts.signTransaction(
            tx,
            "0xf3602b46a63e70180b8c02c79eb7454c88e4805aa40e8e85480eab8173606b26"
        );

        let txReceipt = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);
        console.log(txReceipt);


        if (txReceipt.status) {
            ctx.status = 200;
        } else {
            ctx.status = 400;
        }
        ctx.body = txReceipt.transactionHash;
    }
}