import * as Koa from 'koa';
import { router } from './routes';
import bodyParser = require("koa-bodyparser");

const app = new Koa();
app.use(bodyParser());

app.use(router.routes());
app.listen(3000);

console.log('Server running on port 3000');