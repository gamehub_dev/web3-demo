import * as Router from 'koa-router';
import controller = require('./controller');
import {createReadStream} from "fs";

const router = new Router();

// General
router.get('/', async (ctx) => {
    ctx.type = 'html';
    ctx.body = createReadStream('./app/index.html');
});

// APIs
router.post('/account/create', controller.account.create);
router.post('/account/create-in-node', controller.account.createInNode);
router.get('/account/:address', controller.account.getBalance);

router.post('/withdraw/current', controller.withdraw.withdrawFromCurentNode);
router.post('/withdraw/remote', controller.withdraw.withdrawFromRemoteNode);

export { router };